use clap::Parser;
use indicatif::{ProgressBar, ProgressDrawTarget, ProgressFinish, ProgressStyle};
use std::fs::File;
use std::path::PathBuf;

/// Basic CLI tool for the WUX compression format designed for Wii U disc images
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
enum Cli {
    Decompress(DecompressArgs),
}

/// Decompress a WUX file
#[derive(clap::Args, Debug)]
struct DecompressArgs {
    /// Input WUX file
    input: PathBuf,
    /// Output WUD file
    output: PathBuf,
}

fn decompress(args: DecompressArgs) -> Result<(), wux::Error> {
    let mut input = File::open(args.input)?;
    let mut output = File::create(args.output)?;
    let progress = ProgressBar::with_draw_target(None, ProgressDrawTarget::stderr())
        .with_finish(ProgressFinish::AndLeave)
        .with_style(ProgressStyle::with_template("{bytes}/{total_bytes} {elapsed_precise} [{bytes_per_sec}] {wide_bar} {percent:>3}% ETA {eta_precise}").unwrap());

    wux::decompress_file_with_progress(
        &mut input,
        &mut output,
        |wux::Progress {
             bytes_written,
             total_bytes,
         }| {
            progress.set_length(total_bytes);
            progress.set_position(bytes_written);
        },
    )?;

    progress.finish();
    eprintln!();

    Ok(())
}

fn main() -> Result<(), wux::Error> {
    let cli = Cli::parse();

    let Cli::Decompress(args) = cli;
    decompress(args)?;

    Ok(())
}
